# Changelog

## [Unreleased]

## [1.0.1] - 2022-02-08
### Added
- This CHANGELOG file.

### Changed
- Numeric Goal name is now NumericGoal

### Fixed
- Quests' signals are fixed. They are emit the correct ones.
- Quests no longer crashes when they don't have children.

## [1.0.0] - 2022-01-31
### Added
- Base version.


## Versions
[1.0.1](https://gitlab.com/KatsuroDev/gdquests/-/releases/v1.0.1)
[1.0.0](https://gitlab.com/KatsuroDev/gdquests/-/releases/v1.0.0)