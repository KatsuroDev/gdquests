# GDQuests

## Description
A plugin to handle all types of quest.
Suitable for any type of game with progression.

## Screenshots
![Tree](./screenshots/tree.png)
![Quest inspector](./screenshots/quest.png)

![Extented tree](./screenshots/extented_tree.png)
![Numeric Goal](./screenshots/numeric_goal.png)

## Installation
Download the project and put the addons folder's content inside your project's addons folder. 
If there's no addons folder in your project, create one.

![Addons folder](./screenshots/installation.png)

Make sure that in `Project > Project settings... > Plugins`
GDQuests is enabled. If it isn't there, try reloading the project or verify the installation.
![Enable Plugin](./screenshots/plugin_enabled.png)

## Documentation
If you want to learn how to use the plugin, please refer to the [documentation](./DOCUMENTATION.md).

## Issues
Create issues on this repository if you have any. Features can also be suggested with an issue.

## Contributing
If you have any fixes or features you want to make, PRs will be reviewed for merging.
Make sure you find most bugs you can, and reach out to other developers to check it out first.

## Authors and acknowledgment
- Main developer - KatsuroDev
- Special thanks to Elatronion for supporting and giving out the idea for this plugin!

## License
[MIT License](./LICENSE).

## Credits
- [Icons8](https://icons8.com/)
- [Merge icons created by Maxim Basinski Premium - Flaticon](https://www.flaticon.com/free-icons/merge)
