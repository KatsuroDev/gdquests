# Get Started
Once you get the add-on installed, you'll be able to get all the nodes.
First step is to create a quest! Create a new scene and select the `Quest` node as root.

Now let's set up the goals. Add a new `Node` and call it `Goals`. Make sure it's called exactly like this. Then, you can attach a `Goal` to it. If you want multiple goals, make sure you use a container: the `AND` and the `OR` nodes. 

For the triggers, to make the quest active, follow the same steps as the goals, but call your `Node` `Trigger` instead.

When this is done, you can instance this quest everywhere. If you want something global please continue!

#### Global quests

Create a new scene and use a `Manager` as root. After, you can use `Books` to organise your tree. In a `Book` you can use `Chapters` to hold the quests.
In `Project > Project settings... > Autoload`, add the `Manager` scene. Then you can access it wherever you are in your scripts.

#### Access global triggers/goals

You can easily modify individual `Goal` and `Trigger`'s scripts. Connect them with your nodes via signals. You can also use the `get_node()` function to get a specific goal, this way is easier when you use the global method stated before.

# Nodes


## Goal
**Inherits:** Node < Object

Base class of all nodes in this plugin

#### Description
The basic goal is just a boolean goal. It is used when a certain task can only be either done or undone, not halfway done. For example, talking to a certain character, this goal fits perfectly.

#### Properties
| Type | Name | Default | Description |
| ---- | ---- | ------- | ----------- |
| bool | is_complete | `false` | Keeps track of the goal completion state. |
| String | title | `""` | Title of the goal. |
| String | description | `""` | Description of the goal. |
| bool | is_sticky | `false` | Whether the goal stays completed when completed or not.

#### Methods
| Type | Name | Description |
| ---- | ---- | ----------- |
| void | set_completion(bool new_value) | Setter method for `is_complete`. |

#### Signals
- goal_updated()

Emitted when `is_complete` has been updated somehow.



## NumericGoal
**Inherits:** Goal < Node < Object

A goal that holds a numeric value aside the completion state.

#### Description
Same as the basic goal, but this one's keep track of numbers. For example, if you need to collect 10 flowers, this goal is exactly made for that.

#### Properties
| Type | Name | Default | Description |
| ---- | ---- | ------- | ----------- |
| bool | can_go_over | `false` | If false, the goal is only completed when `x` is equal to `y`, not when over.
| String | x_title | `""` | Title of the numerator. |
| String | y_title | `""` | Title of the denominator. |
| int | x | `0` | Numerator. |
| int | y | `0`| Denominator. |

#### Methods
| Type | Name | Description |
| ---- | ---- | ----------- |
| void | set_numerator(int new_value) | Setter method for `x`. |
| void | set_denominator(int new_value) | Setter method for `y`. |
| void | update() | Called whenever `x` or `y` has been changed. Sets the completion. |
| float | get_completion_percentage() | Returns the quotient of `x` on `y` in float. |



## Trigger
**Inherits:** Goal < Node < Object

A goal that sets a quest status.

#### Description
A specified goal, used to either deactivate or activate a quest.



## AND
**Inherits:** Goal < Node < Object

A goal/trigger/container container.

#### Description
This nodes contains multiple triggers/goals/containers. It makes sure that every child is completed to be complete itself. Goals/Triggers can be used alone, but if more than one is used, it needs a container, the `AND` or the `OR`.

#### Methods
| Type | Name | Description |
| ---- | ---- | ----------- |
| void | _ready() | Connects children's `goal_updated` signal to itself. |
| void | update() | Called whenever a child has been updated. Sets the completion. |
| float | get_completion_percentage() | Returns the quotient of _numbers of completed child_ on _child count_  in float. |



## OR
**Inherits:** Goal < Node < Object

A goal/trigger/container container.

#### Description
This nodes contains multiple triggers/goals/containers. It makes sure that at least one child is completed to be complete itself. Goals/Triggers can be used alone, but if more than one is used, it needs a container, the `AND` or the `OR`.

#### Methods
| Type | Name | Description |
| ---- | ---- | ----------- |
| void | _ready() | Connects children's `goal_updated` signal to itself. |
| void | update() | Called whenever a child has been updated. Sets the completion. |



## Quest
**Inherits:** Goal < Node < Object

The essence of the plugin.

#### Description
A quest is a goal that has a status, and it holds goals and triggers.

#### Enums
| Name | Values |
| ---- | ------ |
| status | `ACTIVE`, `INACTIVE`, `ABANDONED` |

#### Properties
| Type | Name | Default | Description |
| ---- | ---- | ------- | ----------- |
| status | quest_status | `status.INACTIVE` | The current state of the quest. |

#### Methods
| Type | Name | Description |
| ---- | ---- | ----------- |
| void | _ready() | Connects children's `goal_updated` to itself. Both triggers and goals. |
| void | activate() | Sets the state of the quest to `ACTIVE` and emits `activated`.
| void | deactivate() | Sets the state of the quest to `INACTIVE` and emits `deactivated`.
| void | abandon() | Sets the state of the quest to `ABANDONED` and emits `abandoned`.
| void | update() | Called when a child has been updated. Checks for the triggers and if `ACTIVE` sets completion.

#### Signals
- activated()
Emitted when status is set to `ACTIVE`.

- deactivated()
Emitted when status is set to `INACTIVE`.

- abandoned()
Emitted when status is set to `ABANDONED`.



## Chapter
**Inherits:** AND < Goal < Node < Object

A container of quests.

#### Description
It's a specified `AND`  container, that holds quests. 

#### Properties
| Type | Name | Default | Description |
| ---- | ---- | ------- | ----------- |
| bool | is_sticky | `true` | Overriden |



## Books
**Inherits:**  Chapter < AND < Goal < Node < Object

A container of chapters.

#### Description
Same thing as the `Chapter`, but holds chapters instead.



## Manager
**Inherits:** Node < Object

The root of the quest system.

#### Description
Useful when you need quests to stay persistent when changing scene. Autoload this scene and you'll have access the `Manager`'s quests everywhere, anytime.


